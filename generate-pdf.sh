#!/usr/bin/env bash
ROOTDIR=$(cd "$(dirname "$0")" && pwd)
cd "$ROOTDIR"
# Make a soft link so that images can be accessed by docker and included in the PDF
ln -sf docs/images images
docker run --rm \
       --volume "${ROOTDIR}:/data" \
       --user $(id -u):$(id -g) \
       pandoc/extra docs/*.md -o documentation.pdf \
       --table-of-contents \
       --template template
rm images
