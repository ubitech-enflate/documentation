# Useful resources

- [Dataspace Protocol (DSP)](https://docs.internationaldataspaces.org/ids-knowledgebase/v/dataspace-protocol/overview/readme)
- [Dataspace Protocol Conceptual Model](https://docs.internationaldataspaces.org/ids-knowledgebase/v/dataspace-protocol/overview/model)
- [IDS Reference Architecture Manual (RAM) v4](https://docs.internationaldataspaces.org/ids-knowledgebase/v/ids-ram-4/)
- [Eclipse Dataspace Components (EDC) Connector](https://github.com/eclipse-edc/Connector)
- [EDC Extensions by sovity](https://github.com/sovity/edc-extensions/)
- [EDC UI by sovity](https://github.com/sovity/edc-ui)
