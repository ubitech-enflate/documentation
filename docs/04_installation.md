# Installation

## Requirements
- A Virtual Machine with minimum:

    + 4GB of RAM
    + 20GB of free space

- Dependencies to be installed in the VM:

    + Docker and docker-compose
    + openssl
    + keytool

    In a Debian/Ubuntu based system, you may install all the dependencies with the following command:

    ```shell
    sudo apt install docker docker-compose openssl openjdk-17-jre-headless
    ```

## Setup

1. Clone the [connector](https://gitlab.com/ubitech-enflate/connector) repository

2. Place the key (`.key`) and certificate (`.cert`) files you received inside the `secrets` directory

3. Create a keystore file in PKCS12 format

    ```
    cd secrets
    openssl pkcs12 -export \
      -out keystore.p12 \
      -inkey example-connector.key \
      -in example-connector.cert
    ```

    The command will ask you to set an **export password**. Remember the password you set here because you will use it in the next step.

4. Convert the keystore to JKS format

    ```
    password=<your export password>
    # For example
    password=12345678

    # Then run this command
    keytool -importkeystore -srckeystore keystore.p12 \
        -destkeystore keystore.jks \
        -srcstoretype PKCS12 -deststoretype jks \
        -srcstorepass $password -deststorepass $password \
        -srcalias 1 -destalias connectorkey \
        -srckeypass $password -destkeypass $password
    ```

5. Generate a SKI and AKI of `keystore.p12`

    ```shell
    ./secrets/generate_ski_aki.sh secrets/keystore.p12 <export password>
    # For example
    ./secrets/generate_ski_aki.sh secrets/keystore.p12 12345678
    ```

    Keep the output of this command as you will use it in the next step.

    You may re-run the command if needed.

6. Edit `.env`

    - Set connector information such as `MY_EDC_TITLE`, `MY_EDC_DESCRIPTION`, `MY_EDC_CURATOR_URL` and `MY_EDC_CURATOR_NAME` (they will not affect connector functionality).
    - Copy paste the output of the `generate_ski_aki.sh` command in the `EDC_OAUTH_CLIENT_ID` environment variable.
    - Set the `EDC_KEYSTORE_PASSWORD` variable to your keystore export password.
    - Set `MY_EDC_NAME_KEBAB_CASE` according to the connector name (in kebab-case) you chose during registration (03_registration.md), and you have shared with us.
    - Set `CONNECTOR_UI_URL` to the URL the Connector UI is accessible at. Check out the examples and pick the one compatible with your configuration.
    - Set `IDS_BASE_URL` to the URL the Connector Core is accessible at. Check out the examples and pick the one compatible with your configuration.
    - Set `MY_EDC_PROTOCOL` and `MY_EDC_FQDN` to the protocol and domain you use respectively. Together they represent a Connector ID. It is alright if you do not use a specific domain yet, just put the domain you plan to use.
    - Set an arbitrary `FILE_SERVER_API_KEY`. This should be a string like `peanutbutter` or, even better, `JShMnRVSnE31DsAv`.
    - Set `KEYCLOAK_REALM` to the Keycloak realm name that is associated with your connector (you received this from the data space administrator).

## Start and stop

```shell
# Start
docker compose up

# Stop
docker compose down

# Stop and reset database (Warning: Destructive)
docker compose down -v
```

Services that will be started:

- Connector Core
- Connector UI
- Connector Database
- Connector File Server

## Ports

The following ports should be open:

- `11000`: Connector UI
- `11003`: Connector Core
- `11099`: Connector Auth Proxy
- `42000`: File Server

## System Upgrade
Usually a notification email will be sent in order to upgrade the systems and there will be instructions included. Generally the following are the instructions which can be used for the upgrade.
```
1. cd to the connector deployment directory
2. git stash (stashing the changes that you have made to the configuration)
3. git pull
4. git stash pop (or git stash apply) and resolve conflicts if any
5. docker compose down
6. docker compose pull
7. docker compose up -d
``` 
__*Steps 1-4 are needed in case of a configuration update and they could be omitted if there isn't any. Nevertheless it's better to follow all the steps to avoid losing any updates.*__
