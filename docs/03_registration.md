# Registration

Before you join the data space it is necessary to register as a participant.

Send an email to us (vtheodorou@ubitech.eu, ptriantis@ubitech.eu, mfoti@ubitech.eu), letting us know you would like to register.

In your email provide us with the following information:

- Desired connector name in kebab-case
- Planned URL of the connector core that will target your VMs IP and port 11003
- Planned URL of the connector UI that will target your VMs IP and port 11000

Example 1:
```
Connector name: abc-connector
Connector core URL: https://my-abc-connector-core.com
Connector UI URL: https://my-abc-connector-ui.com
```

Example 2:
```
Connector name: abc-connector
Connector core URL: http://my-abc-connector:11003
Connector UI URL: http://my-abc-connector:11000
```

Example 3:
```
Connector name: abc-connector
Connector core URL: http://123.123.123.123:11003
Connector UI URL: http://123.123.123.123:11000
```


You will receive from us:

1. A private key and a certificate
2. Credentials for logging into the Connector UI

The above will be necessary for the Installation process.
