# Usage

This section will describe a data transfer scenario. It assumes you have access to two connectors - one that will act as a data provider and one as a data consumer.

It also assumes the ports for the provider are as follows:

- `11000`: Provider Connector UI
- `11002`: Provider Connector Management
- `11003`: Provider Connector Core
- `42000`: Provider File Server

And for the consumer:

- `22000`: Consumer Connector UI
- `21002`: Consumer Connector Management
- `22003`: Consumer Connector Core
- `42001`: Consumer File Server

## UI

### Pages
- **Dashboard**: The dashboard page of the Connector displays statistics regarding Incoming and Outgoing Data, contracts and assets as well as information about the Connector itself.
- **Catalog Browser**: The catalog browser page displays all available assets in the data space, including the assets created in the same Connector. It retrieves this information from all the connectors that have been registered in the data space.
- **Contracts**: The contracts page shows a list of contracts agreements that have been signed either by the same Connector or other Connectors.
- **Transfer History**: The transfer history page presents a list of transfers (either incoming or outgoing) and their status.
- **Assets**: The assets page displays a list of assets created in the Connector and allows the creation of new assets.
- **Policies**: The assets page displays a list of existing usage policies in the Connector and allows the creation of new policies.
- **Contract Definitions**: The contract definitions page displays a list of contract definitions created in the Connector and allows the creation of new contract definitions. 
- **Incoming Files**: The incoming files page shows a list of files that have been received through the Connector File Server service and allows locally downloading each file directly through the browser. It essentially displays the list of files present in `connector-deployment/files/incoming`.
- **Outgoing Files**: The outgoing files page shows a list of files that have been uploaded to the Connector File Server service and allows locally uploading a file directly through the browser. It essentially displays the list of files present in `connector-deployment/files/outgoing`.

### Data Exchange Steps 

#### Provider

1. Add any files you would like to share inside the `files/outgoing` directory of the `connector-deployment` directory you are using to deploy the connector.

2. Navigate to the Provider Connector UI and log in using the credentials that you received after your registration.

3. Create an asset

    - Click on **Assets**, then **Create asset**
    - Fill in the information in the first section, **General Information**
    - In the second section, **Datasource information** go to URL and click on the form field
    - Select the file you would like to share or enter a REST API Endpoint link
    - Click on **Add additional header** then fill in the following information

        + Header Name: `x-api-key`
        + Header Value: `the-file-server-api-key-you-set-in-env`

    - Click **Create**

4. (Optional) Create a new usage policy

    By default, the connector UI offers the `always-true` usage policy that does not restrict the usage of an asset.

    If you would like to restrict its usage to a certain connector or a certain time-period:

    - Click on **Policies** and then **Create policy**.
    - Fill in the name of the policy in the ID field.
    - Choose a type of usage restriction.

        There are currently 2 available types:
        + Connector Restricted Usage
        + Time Period Restricted Usage

        In the case of:

        + Connector Restricted Usage, enter the desired Connector ID. The Connector ID can be found in the Dashboard page of the related Connector under Connector Properties -> CONNECTOR ID. It differs from the Connector Endpoint.
        + Time Period Restricted Usage, select the desired start and end date using the date picker.

    - Finally, click **Create**. You will be able to attach your new usage policy to your asset in the next step.

5. Create a contract definition

    - Click on **Contract Definitions**, then **Create Contract Definition**
    - Set an ID and choose the access and contract policies

        + `Access policy` determines whether the asset will be visible in the catalog browser of consumer Connectors or not.

        + `Contract policy` determines whether a consumer will be able to negotiate successfully or not for the asset.

    - In Assets, select the asset you created in step 1
    - Click **Create**

#### Consumer

1. Navigate to the Consumer Connector UI and log in using the credentials that you received after your registration.

2. Go to **Catalog Browser**. You should see the asset you created in the Provider Connector steps.

    If you do not see the asset, there is a chance our Providers Info service is misbehaving.

    + Go to the Provider Connector, click on **Dashboard** and copy the Connector Endpoint visible on the right.
    + Go back to the Consumer Connector and paste the endpoint into the **Connector Endpoints** input box.
    + The asset should be visible then.

3. Click on the asset and then click on **Negotiate**.

4. When the negotiation is complete, click on **Close** and then **Contracts**.

5. Click on the contract that was just signed and then click **Transfer**.

6. In the URL input box, fill in the URL of the consumer file server and then the name of the destination file.

    For example: `http://192.168.2.10:42001/destination-file.txt`

7. Click on **Add additional header** then fill in the following information

    + Header Name: `x-api-key`
    + Header Value: `the-file-server-api-key-you-set-in-env`

8. Click on **Initiate Transfer** and wait a moment.

9. When the transfer process is complete, click on **Incoming Files** find the destination file and click the cloud icon to download it locally.

## API

All the actions described in the above steps may also be performed through the Connector management API.

Please refer to the [postman collection](./postman-connector.json) included in this repository.

To use the postman collection:

1. Download it and import it into Postman.

2. Click the imported **Connector sovity EDC MDS (4.1.0)** collection then click on the **Variables** tab. `MACHINE_IP` should be the first variable. You may set it to the local IP of the machine you are working on.

3. Set `PROVIDER_FILE_SERVER_API_KEY` and `CONSUMER_FILE_SERVER_API_KEY` to the values you set in the respective Connector `.env` files.

You are now ready to use the Postman collection.

Data Exchange example:

1. Send a `1 Create Asset` request. This will create an asset with the specified `ASSET_ID` (declared in Variables) that will pull the data from `PROVIDER_EDC_SOURCE_URL` (provider file server) using the `PROVIDER_FILE_SERVER_API_KEY`.

2. Send a `2 Create Simply Policy` request. This will create a simple `ALWAYS_TRUE` policy with the specified `POLICY_ID`, just like the one present by default in all Connectors.

3. Send a `3 Create ContractDefinition` request. This will create a contract definition with the specified `CONTRACT_DEFINITION_ID`, using the `POLICY_ID` as its access and contract policies and associating asset `ASSET_ID` with them.

4. Send a `4 Request Catalog of Provider` request. This will be sent from the side of the **consumer** and return a list of contract offers that have been available inside the `contractOffers` array.

    Each contract offer has an `id`, for example `contract-definition-1:33708694-eff6-4f32-b5e5-d201b3c216a2`. Select and copy the part that comes after the colon (`:`). In this example it would be `33708694-eff6-4f32-b5e5-d201b3c216a2`.

5. Click on `5 Start Negotiation` and go to the **Body** tab.

    - Paste the text you copied in the previous step so that `offerId` looks like this.

        ```
        "offerId": "{{CONTRACT_DEFINITION_ID}}:33708694-eff6-4f32-b5e5-d201b3c216a2",
        ```

    - Send the request. This will start a contract negotiation and return its ID in the response.

    - To check its status, copy the `id` from the response, for example `82714ee9-f34d-4767-84dc-298f3dbecbf0`. Then click on `5 Get ContractNegotiation` and paste it in the request path so that it looks like this:

        ```
        {{CONSUMER_EDC_MANAGEMENT_URL}}/contractnegotiations/82714ee9-f34d-4767-84dc-298f3dbecbf0
        ```

        Then send the request. In the response you should see `"state": "CONFIRMED"`.
        Also each contract agreement has an ID, available in `contractAgreementId`, for example `contract-definition-1:ebe69ac0-b2eb-48f6-a112-a252966b40e8`
        Copy the part that comes after the colon. Here it would be `ebe69ac0-b2eb-48f6-a112-a252966b40e8`.

6. (Optional) You may check information about a contract agreement by clicking on `6 Get Contract Agreement` and pasting the contract agreement ID in the request path so that it looks like this:

    ```
    {{CONSUMER_EDC_MANAGEMENT_URL}}/contractagreements/{{CONTRACT_DEFINITION_ID}}:ebe69ac0-b2eb-48f6-a112-a252966b40e8
    ```

    Then send the request.

7. Click on `7 Start Data Push` and go to the **Body** tab.

    - Paste the text you copied in the previous step so that `contractAgreementId` looks like this.

    ```
    "contractAgreementId": "{{CONTRACT_DEFINITION_ID}}:ebe69ac0-b2eb-48f6-a112-a252966b40e8",
    ```

    - Send the request. After a small wait, check `connector-deployment/files/incoming` and you should see the transferred file `destination-from-postman.txt`.

